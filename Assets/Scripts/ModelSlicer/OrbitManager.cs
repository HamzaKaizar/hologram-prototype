﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class OrbitManager : MonoBehaviour
{
    public static OrbitManager orbit;

    private void Awake()
    {
        if (!orbit)
            orbit = this;
        else
            DestroyImmediate(this);
    }

    private List<OrbitObject> objects;

    private void Start()
    {
        Load3DModels();
    }

    private List<GameObject> loadedObjects;

    private void Load3DModels()
    {
        loadedObjects = new List<GameObject>();
        Addressables.LoadAssetsAsync<GameObject>("3D_model", model =>
        {
            loadedObjects.Add(model);
        }).Completed += (models) => { SetupObjectsInScene(); };
    }

    private void SetupObjectsInScene()
    {
        objects = new List<OrbitObject>();

        for (int i = 0; i < loadedObjects.Count; i++)
        {
            GameObject tempObj = Instantiate(loadedObjects[i]);
            objects.Add(tempObj.GetComponent<OrbitObject>());
        }

        Vector3 startPos = new Vector3(0, objects[currentIndex].transform.position.y, 0);
        objects[currentIndex].transform.position = startPos;
        for (int i = 1; i < objects.Count; i++)
            objects[i].transform.RotateAround(new Vector3(0, 0, radius), Vector3.up, 180);
    }

    public float radius = 2;
    public int speed = 2;
    private int currentIndex = 0;

    public bool CanRotate()
    {
        return objects!=null && objects.Count >= 2;
    }

    public void RotateRight()
    {
        objects[currentIndex].canRevolveRight = true;

        if (currentIndex == 0)
            currentIndex = objects.Count - 1;
        else
            currentIndex--;

        objects[currentIndex].canRevolveRight = true;
    }

    public void RotateLeft()
    {
        objects[currentIndex].canRevolveLeft = true;

        if (currentIndex == objects.Count - 1)
            currentIndex = 0;
        else
            currentIndex++;

        objects[currentIndex].canRevolveLeft = true;
    }
}
