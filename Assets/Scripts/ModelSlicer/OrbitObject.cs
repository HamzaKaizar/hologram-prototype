﻿using System.Collections;
using UnityEngine;

public class OrbitObject : MonoBehaviour
{
    private OrbitManager myOrbit;

    [HideInInspector] public bool canRevolveRight = false, canRevolveLeft = false;

    private void Start()
    {
        myOrbit = OrbitManager.orbit;
    }

    void Update()
    {
        if (canRevolveRight)
            StartCoroutine(RevolveRight());
        if (canRevolveLeft)
            StartCoroutine(RevolveLeft());
    }

    IEnumerator RevolveRight()
    {
        canRevolveRight = false;
        int targetRotation = (int)transform.eulerAngles.y + 180;
        int currentRotation = (int)transform.eulerAngles.y;
        while (currentRotation < targetRotation)
        {
            transform.RotateAround(new Vector3(0, 0, myOrbit.radius), Vector3.up, myOrbit.speed);
            currentRotation += myOrbit.speed;
            yield return new WaitForEndOfFrame();
        }
        transform.eulerAngles = new Vector3(0, targetRotation, 0);
        yield break;
    }

    IEnumerator RevolveLeft()
    {
        canRevolveLeft = false;
        int targetRotation = (int)transform.eulerAngles.y - 180;
        int currentRotation = (int)transform.eulerAngles.y;
        while (currentRotation > targetRotation)
        {
            transform.RotateAround(new Vector3(0, 0, myOrbit.radius), Vector3.up, -myOrbit.speed);
            currentRotation -= myOrbit.speed;
            yield return new WaitForEndOfFrame();
        }
        transform.eulerAngles = new Vector3(0, targetRotation, 0);
        yield break;
    }
}
