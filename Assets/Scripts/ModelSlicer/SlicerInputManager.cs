﻿using UnityEngine;

public class SlicerInputManager : VRControllerInput
{
    private OrbitManager myOrbit;

    private void Start()
    {
        myOrbit = OrbitManager.orbit;
    }

    void Update()
    {
        if (GetUp())
        {
            SliceIn();
        }
        else if (GetDown())
        {
            SliceOut();
        }

        if (myOrbit.CanRotate())
        {
            if (GetRight())
            {
                myOrbit.RotateRight();
            }
            else if (GetLeft())
            {
                myOrbit.RotateLeft();
            }
        }
    }

    public GameObject slicer;
    public Vector2 slicerZBounds;
    private float slicerIncrement = 0.01f;

    private void SliceIn()
    {
        Vector3 offset = new Vector3(0, 0, slicerIncrement);
        if (slicer.transform.position.z < slicerZBounds.y)
            slicer.transform.position += offset;
    }

    private void SliceOut()
    {
        Vector3 offset = new Vector3(0, 0, slicerIncrement);
        if (slicer.transform.position.z > slicerZBounds.x)
            slicer.transform.position -= offset;
    }
}
