﻿using UnityEngine;

public class CanvasResizer : MonoBehaviour
{
    void Start()
    {
        float height = 720f;
        float width = height * ((float)Screen.width / Screen.height);
        Vector2 screenSize = new Vector2(width, height);
        GetComponent<RectTransform>().sizeDelta = screenSize;
    }
}
