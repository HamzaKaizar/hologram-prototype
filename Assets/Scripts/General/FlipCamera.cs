﻿using UnityEngine;

public class FlipCamera : MonoBehaviour
{
    private Camera cam;

    void Start()
    {
        cam = GetComponent<Camera>();
    }

    private void SetCamMatrix(int flip)
    {
        cam.ResetWorldToCameraMatrix();
        cam.ResetProjectionMatrix();
        cam.projectionMatrix *= Matrix4x4.Scale(new Vector3(flip, 1, 1));
    }

    void OnPreCull()
    {
        SetCamMatrix(-1);
    }

    void OnPreRender()
    {
        GL.invertCulling = true;
    }

    void OnPostRender()
    {
        GL.invertCulling = false;
    }

    void OnDisable()
    {
        if (cam)
        {
            SetCamMatrix(1);
        }
    }
}
