﻿using UnityEngine;

public class VRControllerInput : MonoBehaviour
{
    /**
     * JOYSTICK KEYCODES
     * C - Joystick1Button6
     * A - Joystick1Button7
     * B - Joystick1Button0
     * D - Joystick1Button1
     * Trigger Top - Joystick1Button5
     * Trigger Bot - Joystick1Button4
     * */

    private readonly string selectButton = "Submit";
    private readonly string backButton = "Cancel";
    private readonly string settingsButton = "Settings";
    private readonly string flipButton = "Flip";
    private readonly string horizontalAxis = "Horizontal";
    private readonly string verticalAxis = "Vertical";

    private float X, Y;

    public bool GetUp()
    {
        float newY = Input.GetAxisRaw(verticalAxis);

        if (newY == Y)
            return false;

        if (newY > 0)
            return true;
        else
            return false;
    }

    public bool GetDown()
    {
        float newY = Input.GetAxisRaw(verticalAxis);

        if (newY == Y)
            return false;

        if (newY < 0)
            return true;
        else
            return false;
    }

    public bool GetRight()
    {
        float newX = Input.GetAxisRaw(horizontalAxis);

        if (newX == X)
            return false;

        if (newX > 0)
            return true;
        else
            return false;
    }

    public bool GetLeft()
    {
        float newX = Input.GetAxisRaw(horizontalAxis);

        if (newX == X)
            return false;

        if (newX < 0)
            return true;
        else
            return false;
    }

    public bool GetSelectDown()
    {
        return Input.GetButtonDown(selectButton);
    }

    public bool GetBackDown()
    {
        return Input.GetButtonDown(backButton);
    }

    public bool GetSettingsDown()
    {
        return Input.GetButtonDown(settingsButton);
    }

    public bool GetFlipDown()
    {
        return Input.GetButtonDown(flipButton);
    }
}
