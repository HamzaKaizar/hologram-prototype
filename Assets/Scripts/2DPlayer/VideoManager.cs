﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour
{

    private void Start()
    {
        CreateScreen();
        DownloadVideos();
        //PopulatePlaylist();
        //CreateVideoPlayers();
    }

    #region Setup Player

    private RawImage screen;

    //private List<string> videoClipList;
    private List<VideoClip> videos;

    private List<VideoPlayer> videoPlayerList;

    private VideoPlayer singleClipVP;

    private void DownloadVideos()
    {
        videos = new List<VideoClip>();
        Addressables.LoadAssetsAsync<VideoClip>("holo_video", video =>
        {
            videos.Add(video);
        }).Completed += (models) => { CreateVideoPlayers(); };
    }

    private void CreateScreen()
    {
        screen = gameObject.AddComponent<RawImage>();
    }

    private void PopulatePlaylist()
    {
        //videoClipList = new List<string>();
        //string[] files = System.IO.Directory.GetFiles(Application.streamingAssetsPath, "*.mp4");
        //for (int i = 0; i < files.Length; i++)
        //{
        //    videoClipList.Add(files[i]);
        //}
    }

    private void CreateVideoPlayers()
    {
        videoPlayerList = new List<VideoPlayer>();

        List<string> tempNames = new List<string>();

        for (int i = 0; i <= videos.Count; i++)
        {
            GameObject vidHolder = new GameObject("VP" + i);
            vidHolder.transform.SetParent(transform);

            VideoPlayer videoPlayer = vidHolder.AddComponent<VideoPlayer>();
            videoPlayerList.Add(videoPlayer);

            AudioSource audioSource = vidHolder.AddComponent<AudioSource>();

            videoPlayer.playOnAwake = false;
            audioSource.playOnAwake = false;

            videoPlayer.isLooping = false;

            videoPlayer.source = VideoSource.Url;

            videoPlayer.aspectRatio = VideoAspectRatio.Stretch;

            videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

            videoPlayer.EnableAudioTrack(0, true);
            videoPlayer.SetTargetAudioSource(0, audioSource);

            if (i != videos.Count)
            {
                videoPlayer.clip = videos[i];
                tempNames.Add(videos[i].name);
            }
        }
        singleClipVP = videoPlayerList[videos.Count];

        GameObject.FindObjectOfType<PlayerInputManager>().CreatePlaylistButtons(tempNames);
    }

    #endregion

    #region Play Video

    private VideoPlayer currentVideoPlayer;

    private int videoIndex = 0;

    private bool IsPlaylistEmpty()
    {
        return (videos == null || videos.Count <= 0);
    }

    public IEnumerator PlayVideo(bool shouldLoop, int playlistIndex)
    {
        if (IsPlaylistEmpty())
            yield break;

        currentVideoPlayer = singleClipVP;
        currentVideoPlayer.clip = videos[playlistIndex];
        currentVideoPlayer.isLooping = shouldLoop;
        currentVideoPlayer.Prepare();

        yield return new WaitWhile(() => !currentVideoPlayer.isPrepared);

        screen.texture = currentVideoPlayer.texture;
        currentVideoPlayer.Play();
    }

    public IEnumerator PlayAllVideos(bool shouldLoop)
    {
        if (IsPlaylistEmpty())
            yield break;

        if (videoIndex >= videos.Count)
            yield break;

        currentVideoPlayer = videoPlayerList[videoIndex];

        currentVideoPlayer.Prepare();

        yield return new WaitWhile(() => !currentVideoPlayer.isPrepared);

        screen.texture = currentVideoPlayer.texture;

        currentVideoPlayer.Play();

        bool reachedHalfWay = false;
        int nextIndex = (videoIndex + 1);
        while (currentVideoPlayer.isPlaying || isPaused)
        {
            if (!reachedHalfWay && currentVideoPlayer.time >= (videoPlayerList[videoIndex].length / 2))
            {
                reachedHalfWay = true;

                if (nextIndex >= videos.Count)
                {
                    if (shouldLoop)
                        nextIndex = 0;
                    else
                        yield break;
                }

                videoPlayerList[nextIndex].Prepare();
            }
            yield return null;
        }

        yield return new WaitWhile(() => !videoPlayerList[nextIndex].isPrepared);

        videoIndex++;
        if (videoIndex >= videos.Count && shouldLoop)
            videoIndex = 0;

        StartCoroutine(PlayAllVideos(shouldLoop));
    }

    private bool isPaused = false;

    public bool IsPaused()
    {
        return isPaused;
    }

    public void PlayVP()
    {
        isPaused = false;
        currentVideoPlayer.Play();
    }

    public void PauseVP()
    {
        isPaused = true;
        currentVideoPlayer.Pause();
    }

    public void UpdateDisplayPosition(Vector2 newPosition)
    {
        screen.transform.GetComponent<RectTransform>().position = newPosition;
    }

    #endregion
}
