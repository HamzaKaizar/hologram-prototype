﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIElement : MonoBehaviour
{
    public Image highlight;
    public Text label;

    public void OnPointerEnter()
    {
        highlight.enabled = true;
    }

    public void OnPointerExit()
    {
        highlight.enabled = false;
    }
}
