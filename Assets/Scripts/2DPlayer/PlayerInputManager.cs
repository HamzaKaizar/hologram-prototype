﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInputManager : VRControllerInput
{
    private string currentView = "";

    private void Start()
    {
        Cursor.visible = false;
        shouldLoop = true;
    }

    public Camera mainCamera, UICamera;

    public Toggle cameraFlipToggle;

    private int playlistButtonIndex = 0;
    public List<GameObject> playlistButtons;

    private int settingsOptionIndex = 0;
    public List<GameObject> settingsOptions;
    public GameObject settingsHolder;
    private readonly float slideIncrement = 0.03f;

    public GameObject locationHolder, locationGrid;
    private int currentGridX = 0, currentGridY = 0;
    private int savedGridX = 0, savedGridY = 0;
    private readonly int gridX = 16, gridY = 9;
    private readonly float cellSize = 40;

    public RawImage displayScreen;

    public Text buttonClicked;

    private void Update()
    {
        /*//DEBUG KEYCODES
        System.Array values = System.Enum.GetValues(typeof(KeyCode));
        foreach (KeyCode code in values)
        {
            if (Input.GetKeyDown(code))
            {
                buttonClicked.text = System.Enum.GetName(typeof(KeyCode), code);
            }
        }
        */

        SwapCamera();

        if (GetFlipDown())
        {
            FlipCamera();
        }

        if (currentView == "playlist")
        {
            if (GetSelectDown())
            {
                currentView = "player";
                playlistButtons[playlistButtonIndex].GetComponent<Button>().onClick.Invoke();
            }
            else if (GetSettingsDown())
            {
                OpenSettings();
            }

            MoveAlongVerticalAxis(ref playlistButtonIndex, ref playlistButtons);
        }
        else if (currentView == "player")
        {
            if (GetSelectDown())
            {
                if (videoManager.IsPaused())
                    PlayVideo();
                else
                    PauseVideo();
            }
            else if (GetBackDown())
                OpenPlayList();
            else if (GetSettingsDown())
                OpenSettings();
        }
        else if (currentView == "settings")
        {
            if (GetBackDown())
            {
                CloseSettings();
                return;
            }

            if (GetSelectDown() && settingsOptionIndex == settingsOptions.Count - 1)
            {
                currentGridX = savedGridX;
                currentGridY = savedGridY;
                OpenEditLocationMatrix();
                return;
            }

            MoveAlongVerticalAxis(ref settingsOptionIndex, ref settingsOptions);

            if (!settingsOptions[settingsOptionIndex].GetComponent<Slider>())
                return;

            if (GetRight())
            {
                settingsOptions[settingsOptionIndex].GetComponent<Slider>().value += slideIncrement;
            }
            else if (GetLeft())
            {
                settingsOptions[settingsOptionIndex].GetComponent<Slider>().value -= slideIncrement;
            }
        }
        else if (currentView == "location")
        {

            if (GetSelectDown())
            {
                SaveLocationMatrix();
                return;
            }

            if (GetBackDown())
            {
                currentGridX = savedGridX;
                currentGridY = savedGridY;
                Vector2 newPosition = new Vector2(cellSize * currentGridX, cellSize * currentGridY);
                locationGrid.GetComponent<RectTransform>().position = newPosition;
                CloseEditLocationMatrix();
                return;
            }

            MoveInGrid();
            return;
        }
    }

    private void SwapCamera()
    {
        if (currentView == "player")
        {
            UICamera.gameObject.SetActive(false);
            mainCamera.gameObject.SetActive(true);
        }
        else
        {
            mainCamera.gameObject.SetActive(false);
            UICamera.gameObject.SetActive(true);
        }

    }

    public void FlipCamera()
    {
        cameraFlipToggle.isOn = cameraFlipToggle.isOn ? false : true;
    }

    public void OnFlipCameraToggle()
    {
        UICamera.GetComponent<FlipCamera>().enabled = cameraFlipToggle.isOn;
        mainCamera.GetComponent<FlipCamera>().enabled = cameraFlipToggle.isOn;
    }

    private void MoveAlongVerticalAxis(ref int index, ref List<GameObject> items)
    {
        if (GetDown())
        {
            if (index < items.Count - 1)
            {
                items[index].GetComponent<UIElement>().OnPointerExit();
                items[++index].GetComponent<UIElement>().OnPointerEnter();
            }
        }
        else if (GetUp())
        {
            if (index > 0)
            {
                items[index].GetComponent<UIElement>().OnPointerExit();
                items[--index].GetComponent<UIElement>().OnPointerEnter();
            }
        }
    }

    public void OnVolumeChange()
    {
        AudioListener.volume = settingsOptions[settingsOptionIndex].GetComponent<Slider>().value;
    }

    public void OnBrightnessChange()
    {
        float colorVal = settingsOptions[settingsOptionIndex].GetComponent<Slider>().value;
        videoManager.GetComponent<RawImage>().color = new Color(colorVal, colorVal, colorVal);
    }

    public void OnFOVChange()
    {
        float fov = settingsOptions[settingsOptionIndex].GetComponent<Slider>().value;
        mainCamera.fieldOfView = fov;
    }

    private void MoveInGrid()
    {
        float currentX = locationGrid.GetComponent<RectTransform>().position.x;
        float currentY = locationGrid.GetComponent<RectTransform>().position.y;

        if (GetDown())
        {
            if (currentGridY > -gridY + 1)
            {
                Vector2 newPosition = new Vector2(currentX, currentY - cellSize);
                locationGrid.GetComponent<RectTransform>().position = newPosition;
                currentGridY--;
            }
        }
        else if (GetUp())
        {
            if (currentGridY < gridY - 1)
            {
                Vector2 newPosition = new Vector2(currentX, currentY + cellSize);
                locationGrid.GetComponent<RectTransform>().position = newPosition;
                currentGridY++;
            }
        }
        if (GetRight())
        {
            if (currentGridX < gridX - 1)
            {
                Vector2 newPosition = new Vector2(currentX + cellSize, currentY);
                locationGrid.GetComponent<RectTransform>().position = newPosition;
                currentGridX++;
            }
        }
        else if (GetLeft())
        {
            if (currentGridX > -gridX + 1)
            {
                Vector2 newPosition = new Vector2(currentX - cellSize, currentY);
                locationGrid.GetComponent<RectTransform>().position = newPosition;
                currentGridX--;
            }
        }
    }

    public GameObject playlistItem, playlistContent;

    public void CreatePlaylistButtons(List<string> videoNames)
    {
        for (int i = 0; i < videoNames.Count; i++)
        {
            string videoName = videoNames[i];

            GameObject item = Instantiate(playlistItem);
            item.name = videoName;
            item.transform.SetParent(playlistContent.transform);
            item.GetComponent<RectTransform>().localScale = Vector3.one;
            int temp = new int();
            temp = i;
            item.GetComponent<UIElement>().label.text = videoName;
            item.GetComponent<Button>().onClick.AddListener(delegate
            {
                InvokeSelectedVideo(temp);
            });
            playlistButtons.Add(item);
        }

        currentView = "playlist";
        playlistButtons[playlistButtonIndex].GetComponent<UIElement>().OnPointerEnter();
    }

    private bool shouldLoop;

    public void ToggleLoop()
    {

    }

    public VideoManager videoManager;
    public GameObject playlistHolder;
    public Sprite play, pause;
    public Image playPauseButton;

    public void InvokeSelectedVideo(int index)
    {
        playlistHolder.SetActive(false);
        playPauseButton.sprite = pause;
        StartCoroutine(videoManager.PlayVideo(shouldLoop, index));
    }

    public void InvokePlayAllVideos()
    {
        playlistHolder.SetActive(false);
        playPauseButton.sprite = pause;
        StartCoroutine(videoManager.PlayAllVideos(shouldLoop));
    }

    public void OpenPlayList()
    {
        if (currentView == "player")
        {
            PauseVideo();
        }

        currentView = "playlist";
        playlistHolder.SetActive(true);
    }

    public void ClosePlaylist()
    {
        playlistHolder.SetActive(false);
    }

    public void OpenSettings()
    {
        if (currentView == "player")
        {
            PauseVideo();
        }

        currentView = "settings";
        settingsOptions[settingsOptionIndex].GetComponent<UIElement>().OnPointerExit();
        settingsOptionIndex = 0;
        settingsOptions[settingsOptionIndex].GetComponent<UIElement>().OnPointerEnter();
        settingsHolder.SetActive(true);
    }

    public void CloseSettings()
    {
        settingsHolder.SetActive(false);
        if (playlistHolder.activeSelf)
            currentView = "playlist";
        else
        {
            currentView = "player";
            PlayVideo();
        }
    }

    public void OpenEditLocationMatrix()
    {
        settingsHolder.SetActive(false);
        currentView = "location";
        locationHolder.SetActive(true);
    }

    public void SaveLocationMatrix()
    {
        savedGridX = currentGridX;
        savedGridY = currentGridY;
        Vector2 newPos = locationGrid.GetComponent<RectTransform>().position * 2;
        videoManager.UpdateDisplayPosition(newPos);
        CloseEditLocationMatrix();
    }

    public void CloseEditLocationMatrix()
    {
        locationHolder.SetActive(false);
        OpenSettings();
    }

    public void PlayVideo()
    {
        playPauseButton.sprite = pause;
        videoManager.PlayVP();
    }

    public void PauseVideo()
    {
        playPauseButton.sprite = play;
        videoManager.PauseVP();
    }
}
